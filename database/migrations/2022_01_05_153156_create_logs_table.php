<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            /**
             * Set character encoding
             */
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            $table->id();
            $table->enum('level', [
                'info',
                'warning',
                'error',
                'debug',
                'critical',
                'alert',
                'emergency',
                'notice'
            ]);
            $table->string('driver', 16)->default('database');
            $table->string('message', 512)->nullable(false);
            $table->string('trace', 1024)->nullable(false);
            $table->string('channel',16)->nullable(true)->default(null);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('logs');
    }
}

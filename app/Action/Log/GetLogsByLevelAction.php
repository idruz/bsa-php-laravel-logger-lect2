<?php

namespace App\Action\Log;

use App\Repository\LogRepositoryInterface;

class GetLogsByLevelAction
{
    private LogRepositoryInterface $logRepository;

    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    public function execute(string $level): array
    {
        return $this->logRepository->findByLevel($level);
    }
}

<?php

namespace App\Repository;

interface LogRepositoryInterface
{
    public function findAll(): array;
    public function findByLevel(string $level): array;
    public function getLogsStatistic(): array;
}

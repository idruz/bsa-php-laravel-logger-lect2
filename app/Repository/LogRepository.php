<?php

namespace App\Repository;

use App\Models\Log;
use Illuminate\Support\Facades\DB;

class LogRepository implements LogRepositoryInterface
{
    public function findAll(): array
    {
        return Log::all()->toArray();
    }

    public function findByLevel(string $level): array
    {
        return Log::where('level', $level)->get()->toArray();
    }

    public function getLogsStatistic(): array
    {
        $result = array_flip(Log::$availableLevels);
        $result = array_fill_keys(array_keys($result), 0);

        $logStatisticData = Log::select('level',DB::raw('count(*) as total'))
            ->groupBy('level')
            ->get()
            ->toArray();

        if (
            !empty($logStatisticData)
            and !empty($result)
        ) {
            foreach ($logStatisticData as $data) {
                if (isset($result[$data['level']])) {
                    $result[$data['level']] = $data['total'];
                }
            }
        }
        
        return $result;
    }
}

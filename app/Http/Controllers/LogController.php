<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Action\Log\GetAllLogsAction;
use App\Action\Log\GetLogsByLevelAction;
use App\Action\Log\GetLogsStatisticAction;
use Illuminate\Support\Facades\Validator;

class LogController extends Controller
{
    public function logs(GetAllLogsAction $getAllLogsAction)
    {
        return response()->json(['data' => $getAllLogsAction->execute()], Response::HTTP_OK);
    }

    public function logLevel(string $level, GetLogsByLevelAction $getLogsByLevelAction)
    {
        $logByLevel = $getLogsByLevelAction->execute($level);

        return response()->json(['data' => $logByLevel], Response::HTTP_OK);
    }

    public function logStatistic(GetLogsStatisticAction $getLogsStatisticAction)
    {
        return \response()->view(
            'logs',
            ['data' => $getLogsStatisticAction->execute()],
            Response::HTTP_OK
        );
    }
}

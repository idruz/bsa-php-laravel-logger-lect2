<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Log Viewer</title>

    <style>
        :root {
            --info-color: #0080ff;
            --warning-color: #f5ac27;
            --error-color: #fd2424;
            --debug-color: #f3f3f3;
            --critical-color: #810000;
            --alert-color: #ff5900;
            --emergency-color: #005200;
            --notice-color: #9d9c9c;
        }
        body {
            background: lightblue;
            padding: 20px;
            font-family: sans-serif;
        }
        ul {
            list-style: none;
            margin: 0;
            padding: 0;
            max-width: 980px;
            width: 100%;
            margin: 0 auto;
        }
        li {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 10px;
        }
        li:last-child {
            margin-bottom: 0;
        }
        li span:first-child {
            font-weight: bold;
        }
        li .info-color {
            color: var(--info-color);
        }
        li .warning-color {
            color: var(--warning-color);
        }
        li .error-color {
            color: var(--error-color);
        }
        li .debug-color {
            color: var(--debug-color);
        }
        li .critical-color {
            color: var(--critical-color);
        }
        li .alert-color {
            color: var(--alert-color);
        }
        li .emergency-color {
            color: var(--emergency-color);
        }
        li .notice-color {
            color: var(--notice-color);
        }
        h1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <h1>Log Viewer</h1>
    <br>
    @if (empty($data))
        <p>Statistic Log empty...</p>
    @else
        <ul>
        @foreach($data as $key => $value)
            <li>
                <span class="{{$key}}-color">{{$key}}</span>
                <span>{{$value}}</span>
            </li>
        @endforeach
        </ul>
    @endif
</body>
</html>
